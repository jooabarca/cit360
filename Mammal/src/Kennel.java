public class Kennel {


    public DogBean[] buildDogs() {

        DogBean[] dogs = new DogBean[5];
        dogs[0] = new DogBean (4, "brown", 20.6, "poodle", "juan" );
        dogs[1] = new DogBean (4, "black", 12.5, "beagle", "clemo" );
        dogs[2] = new DogBean (4, "white", 8.8, "rottweiler", "muri" );
        dogs[3] = new DogBean (4, "brown", 18.7, "bulldog", "porky" );
        dogs[4] = new DogBean (4, "black", 11.8, "husky", "rory" );

        return dogs;
    }

    public void displayDogs (DogBean[] dogs) {

        for (int i = 0; i < dogs.length; i++ ){
            System.out.println(dogs[i].toString());

        }

    }

    public static void main(String[] args) {

        Kennel kennel = new Kennel();
        DogBean[] allDogs = kennel.buildDogs();
        kennel.displayDogs(allDogs);

    }




}
