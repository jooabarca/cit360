import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;

public class TestMammals {
    @Test
    public void testDogsLegs() {
       DogBean dog1 = new DogBean (4, "brown", 20.6, "poodle", "juan" );
       DogBean dog2 = new DogBean (4, "black", 12.5, "beagle", "clemo" );

        // Test the dogs' legs
        assertEquals(4, dog1.getLegCount());
        assertEquals(4, dog2.getLegCount());
    }
    @Test
    public void testMammalsLegs() {
        MammalBean mammal1 = new MammalBean (4, "white", 8.8);
        MammalBean mammal2 = new MammalBean (4, "brown", 18.7);

        //Test the Mammals' legs
        assertTrue(mammal1.getLegCount() < 5 && mammal1.getLegCount() > 2);
        assertTrue(mammal2.getLegCount() < 5 && mammal2.getLegCount() > 2);
    }
    @Test
    public void testSet() {
        /*Create a Set of Mammal objects.++
        Add several Mammal objects to the Set.++
        Write assertions to ensure the Set contains the correct objects.
        Remove a few Mammal objects from the Set.
        Write assertions to ensure the Set contains the correct objects.*/

        Set set = new HashSet();
        MammalBean mammal1 = new MammalBean (2, "pink", 6.0);
        MammalBean mammal2 = new MammalBean (4, "white", 3.0);
        MammalBean mammal3 = new MammalBean (4, "black", 8.8);

        set.add(mammal1);
        set.add(mammal2);
        set.add(mammal3);

        //Test the Mammals' colors
        assertTrue(mammal1.getHeight() > 0);
        assertTrue(mammal2.getHeight() > 0);
        assertTrue(mammal3.getHeight() > 0);
        assertTrue(set.size() == 3);
    }
    @Test
    public void testMap() {
        /*Test a Map of Dog objects.
        Create a Map<String, Dog> that can be used to find Dog objects by their name.
        Add several Dog objects to the Map.
        Write assertions to ensure the Map contains the correct objects.
        Remove a few Dog objects from the Map.
        Write assertions to ensure the Map contains the correct objects.
        */

        HashMap<String, DogBean> dogNames = new HashMap<String, DogBean>();

        DogBean dog1 = new DogBean (4, "brown", 20.6, "german shepherd", "Jack" );
        DogBean dog2 = new DogBean (4, "black", 12.5, "beagle", "Toby" );
        DogBean dog3 = new DogBean (4, "brown", 20.6, "poodle", "Cachupin" );
        DogBean dog4 = new DogBean (4, "white", 12.5, "bulldog", "Simba" );

        dogNames.put("Jack", dog1);
        dogNames.put("Toby", dog2);
        dogNames.put("Cachupin", dog3);
        dogNames.put("Simba", dog4);

        assertFalse(dog1.getLegCount() == 5);
        assertFalse(dog2.getLegCount() == 5);
        assertFalse(dog3.getLegCount() == 5);
        assertFalse(dog4.getLegCount() == 5);
        assertTrue(dogNames.isEmpty() == false);

        //remove few objects

        dogNames.remove("Cachupin", dog3);
        dogNames.remove("Simba", dog4);

        assertEquals(2, dogNames.size());
    }
}
