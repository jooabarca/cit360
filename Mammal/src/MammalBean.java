public class MammalBean {

    // fields
    private int legCount = 2;
    private String color = "";
    private double height = 0;

    // one constructor
    public MammalBean(int startLegCount, String startColor, double startHeight) {
        legCount = startLegCount;
        color = startColor;
        height = startHeight;
    }

    // getters and setters
    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public int getLegCount() {
        return this.legCount;

    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return this.height;
    }

}