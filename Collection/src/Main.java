import java.io.*;
import java.util.*;

public class Main {
    public static void main(String args[]) throws IOException {
        BufferedReader br = null;
        String line = "";
        String split = ",";


        Map<String, Account> tm = new TreeMap<>();


        try
        {
            br = new BufferedReader(new FileReader("rawData.csv"));
            int count = 0;
            while((line = br.readLine()) != null)
            {
                if (count == 0)
                {
                    count++;
                    continue;
                }

                String[] data = line.split(split);
                if(!tm.containsKey(data[0]))
                {
                    Account account = new Account(data[0], data[1], data[2], data[3]);
                    tm.put(data[0], account);
                }
            }

            for (Map.Entry<String, Account> entry : tm.entrySet())
            {
                System.out.println(entry.getValue().toString());
            }

        }

        finally
        {
            try
            {
                br.close();
            }
            catch (IOException e)
            {
                System.out.println(e);

            }
        }
    }
}


