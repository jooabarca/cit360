public class DogBean extends MammalBean {

    // fields
    private String breed = "";
    private String name = "";

    // a constructor that accepts and sets values for each of the DogBean and MammalBean properties.
    public DogBean(int startLegCount, String startColor, double startHeight, String startBreed, String startName) {
        super(startLegCount, startColor, startHeight);
        breed = startBreed;
        name = startName;
    }

    // getters and setters
    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getBreed() {

        return this.breed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }


    public String toString() {
        return "Name of the dog: " + this.name + ", "
                + "Breed of the dog: " + this.breed + ", " + "number of legs: "
                + getLegCount() + ", " + "color: " + getColor() + ", "
                + "Height: " + getHeight();
    }
}